package main

import (
	"fmt"
	"os"

	"github.com/go-redis/redis"
)

func main() {
	redis_client := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
	})

	if err := redis_client.Ping().Err(); err != nil {
		panic("Unable to connect to Redis: " + err.Error())
	}

	val, err := redis_client.Info("server").Result()
	if err != nil {
		panic(err)
	}

	fmt.Print(val)

	if len(os.Args) < 2 {
		panic("Bey")
	}

	res := redis_client.Incr(os.Args[1])
	fmt.Println(res.Val())

	fmt.Println(os.Args[2:])

	for k, v := range os.Args[2:] {
		//	fmt.Printf("%d-%s\n", k, v)
		key := fmt.Sprintf("%s-%d-%d", os.Args[1], res.Val(), k)
		err := redis_client.Set(key, v, 0).Err()
		if err != nil {
			panic(err)
		}
	}
}
